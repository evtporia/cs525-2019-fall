(* ****** ****** *)

#staload "./mylib.sats"

(* ****** ****** *)

#include
"share/atspre_staload.hats"
(*
#include
"share/atspre_staload_libats_ML.hats"
*)

(* ****** ****** *)

implement
{a}
mylist_length
  (xs) =
( loop(xs, 0) ) where
{
//
fun
loop
(xs: mylist(a), n: int) =
case+ xs of
| mylist_nil() => 0
| mylist_cons(_, xs) => loop(xs, n+1)
//
} (* end of [mylist_length] *)

(* ****** ****** *)

(* end of [mylib.dats] *)
