datatype
mylist(a:t0ype) =
| mylist_nil of ()
| mylist_cons of (a, mylist(a))

fun
{a:t0ype}
mylist_length(xs: mylist(a)): int
