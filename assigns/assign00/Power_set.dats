(*
//
// Here is a C program
//
// http://rosettacode.org/wiki/Power_set#C
//
*)

(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)
//
// HX: 10 points
//
(* ****** ****** *)
//
extern
fun
Power_set(xs: list0(int)): void
//
(* ****** ****** *)

(* end of [Power_set.dats] *)
